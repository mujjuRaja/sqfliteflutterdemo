
import 'dart:async';

import 'package:sqlitedatabase_app/database/DatabaseHelper.dart';
import 'package:sqlitedatabase_app/database/model/user.dart';
import 'package:sqlitedatabase_app/database/model/userName.dart';


abstract class HomeContract {
  void screenUpdate();
}

class HomePresenter {

  HomeContract _view;

  var db = new DatabaseHelper();

  HomePresenter(this._view);


  delete(User user) {
    var db = new DatabaseHelper();
    db.deleteUsers(user);
    updateScreen();
  }

  Future<List<User>> getUser() {
    return db.getUser();
  }
  Future<List<UserName>> getUserName() {
    return db.getUserName();
  }

  updateScreen() {
    _view.screenUpdate();

  }


}