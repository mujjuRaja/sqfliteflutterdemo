
class UserName {

  int id;
  String _name;

  UserName(this._name);

  UserName.map(dynamic obj) {
    this._name = obj["name"];
  }

  String get name => _name;


  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();

    map["name"] = _name;
    return map;
  }

  void setUserId(int id) {
    this.id = id;
  }
}