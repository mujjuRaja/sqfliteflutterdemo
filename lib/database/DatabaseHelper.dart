import 'dart:async';
import 'dart:io' as io;

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqlitedatabase_app/database/Constant.dart';
import 'package:sqlitedatabase_app/database/model/user.dart';
import 'package:sqlitedatabase_app/database/model/userName.dart';

//Main Database helper class
class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    //MM: Set database name as per requiredment
    String path = join(documentsDirectory.path, Constant.databaseName);
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }


  static String KEY_ID = "id";
  static String KEY_FIRST_NAME = "firstname";
  static String KEY_LAST_NAME = "lastname";
  static String KEY_DOB = "dob";

  String CREATE_USER_TABLE = "CREATE TABLE " +
      Constant.userTable +
      "(" +
      KEY_ID +
      " INTEGER PRIMARY KEY AUTOINCREMENT," +
      KEY_FIRST_NAME +
      " TEXT NOT NULL," +
      KEY_LAST_NAME +
      " TEXT NOT NULL," +
      KEY_DOB +
      " TEXT NOT NULL," +
      ")";


  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
   //Possible to Create multiple table
    await db.execute(CREATE_USER_TABLE);
    await db.execute('CREATE TABLE mytable(id INTEGER PRIMARY KEY, name TEXT)');
  }

  Future<int> saveUser(User user) async {
    var dbClient = await db;
    int res = await dbClient.insert(Constant.userTable, user.toMap());
    return res;
  }

  Future<int> saveUserName(UserName user) async {
    var dbClient = await db;
    int res = await dbClient.insert("mytable", user.toMap());
    return res;
  }



  Future<List<User>> getUser() async {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM User');
    List<User> employees = new List();
    for (int i = 0; i < list.length; i++) {
      var user =
      new User(list[i][KEY_FIRST_NAME], list[i][KEY_LAST_NAME], list[i][KEY_DOB]);
      user.setUserId(list[i][KEY_ID]);
      employees.add(user);
    }
    print(employees.length);
    return employees;
  }

  Future<List<UserName>> getUserName() async {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM mytable');
    List<UserName> employeesName = new List();
    for (int i = 0; i < list.length; i++) {
      var user =
      new UserName(list[i]["name"]);
      user.setUserId(list[i]["id"]);
      employeesName.add(user);
    }
    print(employeesName.length);
    return employeesName;
  }



  Future<int> deleteUsers(User user) async {
    var dbClient = await db;

    int res =
    await dbClient.rawDelete('DELETE FROM User WHERE id = ?', [user.id]);
    return res;
  }

  Future<bool> update(User user) async {
    var dbClient = await db;

    int res =   await dbClient.update(Constant.userTable, user.toMap(), where: "id = ?", whereArgs: <int>[user.id]);

    return res > 0 ? true : false;
  }
}